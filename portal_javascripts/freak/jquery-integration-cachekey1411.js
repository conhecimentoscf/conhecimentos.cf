
/* Merged Plone Javascript file
 * This file is dynamically assembled from separate parts.
 * Some of these parts have 3rd party licenses or copyright information attached
 * Such information is valid for that section,
 * not for the entire composite file
 * originating files are separated by - filename.js -
 */

/* - jquery-integration.js - */
// https://www.conhecimentos.cf/portal_javascripts/jquery-integration.js?original=1
var jq=jQuery.noConflict();if(typeof cssQuery=='undefined'){
function cssQuery(s,f){return jq.makeArray(jq(s,f))}};

/* - register_function.js - */
// https://www.conhecimentos.cf/portal_javascripts/register_function.js?original=1
var bugRiddenCrashPronePieceOfJunk=(navigator.userAgent.indexOf('MSIE 5')!=-1&&navigator.userAgent.indexOf('Mac')!=-1)
var W3CDOM=(!bugRiddenCrashPronePieceOfJunk&&typeof document.getElementsByTagName!='undefined'&&typeof document.createElement!='undefined');var registerEventListener=function(elem,event,func){jq(elem).bind(event,func)}
var unRegisterEventListener=function(elem,event,func){jq(elem).unbind(event,func)}
var registerPloneFunction=jq;
function getContentArea(){var node=jq('#region-content,#content');return node.length?node[0]:null}


/* - plone_javascript_variables.js - */
// https://www.conhecimentos.cf/portal_javascripts/plone_javascript_variables.js?original=1
var portal_url='https://www.conhecimentos.cf';var form_modified_message='O seu formulário não foi salvo. Todas as alterações efetuadas serão perdidas';var form_resubmit_message='Você já clicou no botão de envio. Você realmente quer enviar este formulário novamente?';var external_links_open_new_window='false';var mark_special_links='false';

/* - nodeutilities.js - */
// https://www.conhecimentos.cf/portal_javascripts/nodeutilities.js?original=1
function wrapNode(node,wrappertype,wrapperclass){jq(node).wrap('<'+wrappertype+'>').parent().addClass(wrapperclass)};
function nodeContained(innernode,outernode){return jq(innernode).parents().filter(function(){return this==outernode}).length>0};
function findContainer(node,func){p=jq(node).parents().filter(func);return p.length?p.get(0):false};
function hasClassName(node,class_name){return jq(node).hasClass(class_name)};
function addClassName(node,class_name){jq(node).addClass(class_name)};
function removeClassName(node,class_name){jq(node).removeClass(class_name)};
function replaceClassName(node,old_class,new_class,ignore_missing){if(ignore_missing||jq(node).hasClass(old_class))
jq(node).removeClass(old_class).addClass(new_class)};
function walkTextNodes(node,func,data){jq(node).find('*').andSelf().contents().each(function(){if(this.nodeType==3) func(this,data)})};
function getInnerTextCompatible(node){return jq(node).text()};
function getInnerTextFast(node){return jq(node).text()};
function sortNodes(nodes,fetch_func,cmp_func){var SortNodeWrapper=function(node){this.value=fetch_func(node);this.cloned_node=node.cloneNode(true)}
SortNodeWrapper.prototype.toString=function(){return this.value.toString?this.value.toString():this.value}
var items=jq(nodes).map(function(){return new SortNodeWrapper(this)});if(cmp_func) items.sort(cmp_func);else items.sort();jq.each(items, function(i){jq(nodes[i]).replace(this.cloned_node)})};
function copyChildNodes(srcNode,dstNode){jq(srcNode).children().clone().appendTo(jq(dstNode))}


/* - cookie_functions.js - */
// https://www.conhecimentos.cf/portal_javascripts/cookie_functions.js?original=1
function createCookie(name,value,days){if(days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString()} else{expires=""}
document.cookie=name+"="+escape(value)+expires+"; path=/;"};
function readCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==' '){c=c.substring(1,c.length)}
if(c.indexOf(nameEQ)==0){return unescape(c.substring(nameEQ.length,c.length))}}
return null};

/* - livesearch.js - */
// https://www.conhecimentos.cf/portal_javascripts/livesearch.js?original=1
var livesearch=function(){var _2=400;var _7=400;var _0={};var _1="LSHighlight";function _5(f,i){var l=null;var r=null;var c={};var q="livesearch_reply";var q=f.attr('action').replace(/search$/g,"")+q;var re=f.find('div.LSResult');var s=f.find('div.LSShadow');var p=f.find('input[name=path]');function _12(){re.hide();l=null};function _6(){window.setTimeout('livesearch.hide("'+f.attr('id')+'")',_7)};function _11(d){re.show();s.html(d)};function _14(){if(l==i.value){return}l=i.value;if(r&&r.readyState<4)r.abort();if(i.value.length<2){_12();return}var qu={q:i.value};if(p.length&&p[0].checked)qu['path']=p.val();qu=jq.param(qu);if(c[qu]){_11(c[qu]);return}r=jq.get(q,qu,function(d){_11(d);c[qu]=d},'text')};function _4(){window.setTimeout('livesearch.search("'+f.attr('id')+'")',_2)};return{hide:_12,hide_delayed:_6,search:_14,search_delayed:_4}};function _3(f){var t=null;var re=f.find('div.LSResult');var s=f.find('div.LSShadow');function _16(){c=s.find('li.LSHighlight').removeClass(_1);p=c.prev('li');if(!p.length)p=s.find('li:last');p.addClass(_1);return false};function _9(){c=s.find('li.LSHighlight').removeClass(_1);n=c.next('li');if(!n.length)n=s.find('li:first');n.addClass(_1);return false};function _8(){s.find('li.LSHighlight').removeClass(_1);re.hide()};function _10(e){window.clearTimeout(t);switch(e.keyCode){case 38:return _16();case 40:return _9();case 27:return _8();case 37:break;case 39:break;default:{t=window.setTimeout('livesearch.search("'+f.attr('id')+'")',_2)}}};function _13(){var t=s.find('li.LSHighlight a').attr('href');if(!t)return;window.location=t;return false};return{handler:_10,submit:_13}};function _15(i){var i='livesearch'+i;var f=jq(this).parents('form:first');var k=_3(f);_0[i]=_5(f,this);f.attr('id',i).submit(k.submit);jq(this).attr('autocomplete','off').keydown(k.handler).focus(_0[i].search_delayed).blur(_0[i].hide_delayed)};jq(function(){jq("#searchGadget,input.portlet-search-gadget").each(_15)});return{search:function(id){_0[id].search()},hide:function(id){_0[id].hide()}}}();

/* - select_all.js - */
// https://www.conhecimentos.cf/portal_javascripts/select_all.js?original=1
function toggleSelect(selectbutton,id,initialState,formName){id=id||'ids:list'
var state=selectbutton.isSelected;state=state==null?Boolean(initialState):state;selectbutton.isSelected=!state;jq(selectbutton).attr('src',portal_url+'/select_'+(state?'all':'none')+'_icon.gif');var base=formName?jq(document.forms[formName]):jq(document);base.find(':checkbox[name='+id+']').attr('checked',!state)}


/* - dragdropreorder.js - */
// https://www.conhecimentos.cf/portal_javascripts/dragdropreorder.js?original=1
var ploneDnDReorder={};ploneDnDReorder.dragging=null;ploneDnDReorder.table=null;ploneDnDReorder.rows=null;ploneDnDReorder.doDown=function(e){var dragging=jq(this).parents('.draggable:first');if(!dragging.length) return;ploneDnDReorder.rows.mousemove(ploneDnDReorder.doDrag);ploneDnDReorder.dragging=dragging;dragging._position=ploneDnDReorder.getPos(dragging);dragging.addClass("dragging");return false};ploneDnDReorder.getPos=function(node){var pos=node.parent().children('.draggable').index(node[0]);return pos==-1?null:pos};ploneDnDReorder.doDrag=function(e){var dragging=ploneDnDReorder.dragging;if(!dragging) return;var target=this;if(!target) return;if(jq(target).attr('id')!=dragging.attr('id')){ploneDnDReorder.swapElements(jq(target),dragging)};return false};ploneDnDReorder.swapElements=function(child1,child2){var parent=child1.parent();var items=parent.children('[id]');items.removeClass('even').removeClass('odd');if(child1[0].swapNode){child1[0].swapNode(child2[0])} else{var t=parent[0].insertBefore(document.createTextNode(''),child1[0]);child1.insertBefore(child2);child2.insertBefore(t);jq(t).remove()};parent.children('[id]:odd').addClass('even');parent.children('[id]:even').addClass('odd')};ploneDnDReorder.doUp=function(e){var dragging=ploneDnDReorder.dragging;if(!dragging) return;dragging.removeClass("dragging");ploneDnDReorder.updatePositionOnServer();dragging._position=null;try{delete dragging._position} catch(e){};dragging=null;ploneDnDReorder.rows.unbind('mousemove',ploneDnDReorder.doDrag);return false};ploneDnDReorder.updatePositionOnServer=function(){var dragging=ploneDnDReorder.dragging;if(!dragging) return;var delta=ploneDnDReorder.getPos(dragging)-dragging._position;if(delta==0){return};var args={item_id:dragging.attr('id').substr('folder-contents-item-'.length)};args['delta:int']=delta;jQuery.post('folder_moveitem',args)};

/* - collapsiblesections.js - */
// https://www.conhecimentos.cf/portal_javascripts/collapsiblesections.js?original=1
function activateCollapsibles(){jq('dl.collapsible:not([class$=Collapsible])').find('dt.collapsibleHeader:first').click(function(){var c=jq(this).parents('dl.collapsible:first');if(!c)return true;var t=c.hasClass('inline')?'Inline':'Block';c.toggleClass('collapsed'+t+'Collapsible').toggleClass('expanded'+t+'Collapsible')}).end().each(function(){var s=jq(this).hasClass('collapsedOnLoad')?'collapsed':'expanded';var t=jq(this).hasClass('inline')?'Inline':'Block';jq(this).removeClass('collapsedOnLoad').addClass(s+t+'Collapsible')})};jq(activateCollapsibles);

/* - form_tabbing.js - */
// https://www.conhecimentos.cf/portal_javascripts/form_tabbing.js?original=1
var ploneFormTabbing={};ploneFormTabbing._toggleFactory=function(container,tab_ids,panel_ids){return function(e){jq(tab_ids).removeClass('selected');jq(panel_ids).addClass('hidden');var orig_id=this.tagName.toLowerCase()=='a'?'#'+this.id:jq(this).val();var id=orig_id.replace(/^#fieldsetlegend-/,"#fieldset-");jq(orig_id).addClass('selected');jq(id).removeClass('hidden');jq(container).find("input[name=fieldset.current]").val(orig_id);return false}};ploneFormTabbing._buildTabs=function(container,legends){var threshold=6;var tab_ids=[];var panel_ids=[];legends.each(function(i){tab_ids[i]='#'+this.id;panel_ids[i]=tab_ids[i].replace(/^#fieldsetlegend-/,"#fieldset-")});var handler=ploneFormTabbing._toggleFactory(container,tab_ids.join(','),panel_ids.join(','));if(legends.length>threshold){var tabs=document.createElement("select");var tabtype='option';jq(tabs).change(handler).addClass('noUnloadProtection')} else{var tabs=document.createElement("ul");var tabtype='li'}
jq(tabs).addClass('formTabs');legends.each(function(){var tab=document.createElement(tabtype);jq(tab).addClass('formTab');if(legends.length>threshold){jq(tab).text(jq(this).text());tab.id=this.id;tab.value='#'+this.id} else{var a=document.createElement("a");a.id=this.id;a.href="#"+this.id;jq(a).click(handler);var span=document.createElement("span");jq(span).text(jq(this).text());a.appendChild(span);tab.appendChild(a)}
tabs.appendChild(tab);jq(this).remove()});jq(tabs).children(':first').addClass('firstFormTab');jq(tabs).children(':last').addClass('lastFormTab');return tabs};ploneFormTabbing.select=function($which){if(typeof $which=="string")
$which=jq($which.replace(/^#fieldset-/,"#fieldsetlegend-"));if($which[0].tagName.toLowerCase()=='a'){$which.click();return true} else if($which[0].tagName.toLowerCase()=='option'){$which.attr('selected',true);$which.parent().change();return true} else{$which.change();return true}
return false};ploneFormTabbing.initializeDL=function(){var tabs=jq(ploneFormTabbing._buildTabs(this,jq(this).children('dt')));jq(this).before(tabs);jq(this).children('dd').addClass('formPanel');tabs=tabs.find('li.formTab a,option.formTab');if(tabs.length)
ploneFormTabbing.select(tabs.filter(':first'))};ploneFormTabbing.initializeForm=function(){var fieldsets=jq(this).children('fieldset');if(!fieldsets.length) return;var tabs=ploneFormTabbing._buildTabs(this,fieldsets.children('legend'));jq(this).prepend(tabs);fieldsets.addClass("formPanel");jq(this).find('input[name=fieldset.current]').addClass('noUnloadProtection');var tab_inited=false;jq(this).find('.formPanel:has(div.field.error)').each(function(){var id=this.id.replace(/^fieldset-/,"#fieldsetlegend-");var tab=jq(id);tab.addClass("notify");if(tab.length&&!tab_inited)
tab_inited=ploneFormTabbing.select(tab)});jq(this).find('.formPanel:has(div.field span.fieldRequired)').each(function(){var id=this.id.replace(/^fieldset-/,"#fieldsetlegend-");jq(id).addClass('required')});if(!tab_inited){jq('input[name=fieldset.current][value^=#]').each(function(){tab_inited=ploneFormTabbing.select(jq(this).val())})}
if(!tab_inited){var tabs=jq("form.enableFormTabbing li.formTab a,"+"form.enableFormTabbing option.formTab,"+"div.enableFormTabbing li.formTab a,"+"div.enableFormTabbing option.formTab");if(tabs.length)
ploneFormTabbing.select(tabs.filter(':first'))}
jq("#archetypes-schemata-links").addClass('hiddenStructure');jq("div.formControls input[name=form.button.previous],"+"div.formControls input[name=form.button.next]").remove()};jq(function(){jq("form.enableFormTabbing,div.enableFormTabbing").each(ploneFormTabbing.initializeForm);jq("dl.enableFormTabbing").each(ploneFormTabbing.initializeDL);if(window.location.hash&&jq(".enableFormTabbing "+window.location.hash).length){ploneFormTabbing.select(window.location.hash)}});

/* - input-label.js - */
// https://www.conhecimentos.cf/portal_javascripts/input-label.js?original=1
var ploneInputLabel={focus: function(){var t=jq(this);if(t.hasClass('inputLabelActive')&&t.val()==t.attr('title'))
t.val('').removeClass('inputLabelActive');if(t.hasClass('inputLabelPassword'))
ploneInputLabel._setInputType(t.removeClass('inputLabelPassword'),'password').focus().bind('blur.ploneInputLabel',ploneInputLabel.blur)},blur: function(e){var t=jq(this);if(t.is(':password[value=""]')){t=ploneInputLabel._setInputType(this,'text').addClass('inputLabelPassword').bind('focus.ploneInputLabel',ploneInputLabel.focus);if(e.originalEvent&&e.originalEvent.explicitOriginalTarget)
jq(e.originalEvent.explicitOriginalTarget).trigger('focus!')}
if(!t.val())
t.addClass('inputLabelActive').val(t.attr('title'))},submit: function(){jq('input[title].inputLabelActive').trigger('focus.ploneInputLabel')},_setInputType: function(elem,ntype){var otype=new RegExp('type="?'+jq(elem).attr('type')+'"?')
var nelem=jq(jq('<div></div>').append(jq(elem).clone()).html().replace(otype,'').replace(/\/?>/,'type="'+ntype+'" />'));jq(elem).replaceWith(nelem);return nelem}};jq(function(){jq('form:has(input[title].inputLabel)').submit(ploneInputLabel.submit);jq('input[title].inputLabel').bind('focus.ploneInputLabel',ploneInputLabel.focus).bind('blur.ploneInputLabel',ploneInputLabel.blur).trigger('blur.ploneInputLabel')});

/* - highlightsearchterms.js - */
// https://www.conhecimentos.cf/portal_javascripts/highlightsearchterms.js?original=1
function highlightTermInNode(node,word){var contents=node.nodeValue;if(jq(node).parent().hasClass("highlightedSearchTerm")) return;var highlight=function(content){return jq('<span class="highlightedSearchTerm">'+content+'</span>')}
while(contents&&(index=contents.toLowerCase().indexOf(word))>-1){jq(node).before(document.createTextNode(contents.substr(0,index))).before(highlight(contents.substr(index,word.length))).before(document.createTextNode(contents.substr(index+word.length)));var next=node.previousSibling;jq(node).remove();node=next;contents=node.nodeValue}}
function highlightSearchTerms(terms,startnode){if(!terms||!startnode) return;jq.each(terms, function(i,term){term=term.toLowerCase();if(!term||/(not|and|or)/.test(term)) return;jq(startnode).find('*').andSelf().contents().each(function(){if(this.nodeType==3) highlightTermInNode(this,term)})})}
function getSearchTermsFromURI(uri){var query;if(typeof decodeURI!='undefined'){query=decodeURI(uri)} else if(typeof unescape!='undefined'){query=unescape(uri)} else{}
var result=new Array();if(window.decodeReferrer){var referrerSearch=decodeReferrer();if(null!=referrerSearch&&referrerSearch.length>0){result=referrerSearch}}
var qfinder=new RegExp("(searchterm|SearchableText)=([^&]*)","gi");var qq=qfinder.exec(query);if(qq&&qq[2]){var terms=qq[2].replace(/\+/g,' ').split(' ');result.push.apply(result,jq.grep(terms, function(a){return a!=""}));return result}
return result.length==0?false:result}
jq(function(){var terms=getSearchTermsFromURI(window.location.search);highlightSearchTerms(terms,getContentArea())});

/* - se-highlight.js - */
// https://www.conhecimentos.cf/portal_javascripts/se-highlight.js?original=1
var searchEngines=[['^http://([^.]+\\.)?google.*','q='],['^http://search\\.yahoo.*','p='],['^http://search\\.msn.*','q='],['^http://search\\.aol.*','userQuery='],['^http://(www\\.)?altavista.*','q='],['^http://(www\\.)?feedster.*','q='],['^http://search\\.lycos.*','query='],['^http://(www\\.)?alltheweb.*','q='],['^http://(www\\.)?ask\\.com.*','q=']]
function decodeReferrer(ref){if(null==ref&&document.referrer){ref=document.referrer}
if(!ref) return null;var match=new RegExp('');var seQuery='';for(var i=0;i<searchEngines.length;i++){if(!match.compile){match=new RegExp(searchEngines[i][0],'i')} else{match.compile(searchEngines[i][0],'i')}
if(ref.match(match)){if(!match.compile){match=new RegExp('^.*[?&]'+searchEngines[i][1]+'([^&]+)&?.*$','i')} else{match.compile('^.*[?&]'+searchEngines[i][1]+'([^&]+)&?.*$')}
seQuery=ref.replace(match,'$1');if(seQuery){seQuery=decodeURIComponent(seQuery);seQuery=seQuery.replace(/\'|"/, '');return seQuery.split(/[\s,\+\.]+/)}}}
return null}


/* - first_input_focus.js - */
// https://www.conhecimentos.cf/portal_javascripts/first_input_focus.js?original=1
jq(function(){if(jq("form div.error :input:first").focus().length) return;jq("form.enableAutoFocus :input:not(.formTabs):visible:first").focus()});

/* - accessibility.js - */
// https://www.conhecimentos.cf/portal_javascripts/accessibility.js?original=1
function setBaseFontSize(f,r){var b=jq('body');if(r){b.removeClass('smallText').removeClass('largeText');createCookie("fontsize",f,365)}b.addClass(f)};jq(function(){var f=readCookie("fontsize");if(f)setBaseFontSize(f,0)});

/* - styleswitcher.js - */
// https://www.conhecimentos.cf/portal_javascripts/styleswitcher.js?original=1
function setActiveStyleSheet(title,reset){jq('link[rel*=style][title]').attr('disabled',true).find('[title='+title+']').attr('disabled',false);if(reset) createCookie("wstyle",title,365)};jq(function(){var style=readCookie("wstyle");if(style!=null) setActiveStyleSheet(style,0)});

/* - toc.js - */
// https://www.conhecimentos.cf/portal_javascripts/toc.js?original=1
jq(function(){var dest=jq('dl.toc dd.portletItem');var content=getContentArea();if(!content||!dest.length) return;dest.empty();var location=window.location.href;if(window.location.hash)
location=location.substring(0,location.lastIndexOf(window.location.hash));var stack=[];jq(content).find('*').not('.comment > h3').filter(function(){return/^h[1234]$/.test(this.tagName.toLowerCase())}).not('.documentFirstHeading').each(function(i){var level=this.nodeName.charAt(1)-1;while(stack.length<level){var ol=jq('<ol>');if(stack.length){var li=jq(stack[stack.length-1]).children('li:last');if(!li.length)
li=jq('<li>').appendTo(jq(stack[stack.length-1]));li.append(ol)}
stack.push(ol)}
while(stack.length>level) stack.pop();jq(this).before(jq('<a name="section-'+i+'" />'));jq('<li>').append(jq('<a />').attr('href',location+'#section-'+i).text(jq(this).text())).appendTo(jq(stack[stack.length-1]))});if(stack.length){jq('dl.toc').show();oltoc=jq(stack[0]);numdigits=oltoc.children().length.toString().length;oltoc.addClass("TOC"+numdigits+"Digit");dest.append(oltoc);var wlh=window.location.hash;if(wlh){var $target=jq(wlh);$target=$target.length&&$target||jq('[name='+wlh.slice(1)+']');var targetOffset=$target.offset().top;jq('html,body').animate({scrollTop:targetOffset},0)}}});
