
/* Merged Plone Javascript file
 * This file is dynamically assembled from separate parts.
 * Some of these parts have 3rd party licenses or copyright information attached
 * Such information is valid for that section,
 * not for the entire composite file
 * originating files are separated by - filename.js -
 */

/* - ufo.js - */
// https://www.conhecimentos.cf/portal_javascripts/ufo.js?original=1
var UFO={req:["movie","width","height","majorversion","build"],opt:["play","loop","menu","quality","scale","salign","wmode","bgcolor","base","flashvars","devicefont","allowscriptaccess","seamlesstabbing","allowfullscreen","allownetworking"],optAtt:["id","name","align"],optExc:["swliveconnect"],ximovie:"ufo.swf",xiwidth:"215",xiheight:"138",ua:navigator.userAgent.toLowerCase(),pluginType:"",fv:[0,0],foList:[],create: function(FO,id){if(!UFO.uaHas("w3cdom")||UFO.uaHas("ieMac")) return;UFO.getFlashVersion();UFO.foList[id]=UFO.updateFO(FO);UFO.createCSS("#"+id,"visibility:hidden;");UFO.domLoad(id)},updateFO: function(FO){if(typeof FO.xi!="undefined"&&FO.xi=="true"){if(typeof FO.ximovie=="undefined") FO.ximovie=UFO.ximovie;if(typeof FO.xiwidth=="undefined") FO.xiwidth=UFO.xiwidth;if(typeof FO.xiheight=="undefined") FO.xiheight=UFO.xiheight}
FO.mainCalled=false;return FO},domLoad: function(id){var _t=setInterval(function(){if((document.getElementsByTagName("body")[0]!=null||document.body!=null)&&document.getElementById(id)!=null){UFO.main(id);clearInterval(_t)}},250);if(typeof document.addEventListener!="undefined"){document.addEventListener("DOMContentLoaded", function(){UFO.main(id);clearInterval(_t)},null)}},main: function(id){var _fo=UFO.foList[id];if(_fo.mainCalled) return;UFO.foList[id].mainCalled=true;document.getElementById(id).style.visibility="hidden";if(UFO.hasRequired(id)){if(UFO.hasFlashVersion(parseInt(_fo.majorversion,10),parseInt(_fo.build,10))){if(typeof _fo.setcontainercss!="undefined"&&_fo.setcontainercss=="true") UFO.setContainerCSS(id);UFO.writeSWF(id)}
else if(_fo.xi=="true"&&UFO.hasFlashVersion(6,65)){UFO.createDialog(id)}}
document.getElementById(id).style.visibility="visible"},createCSS: function(selector,declaration){var _h=document.getElementsByTagName("head")[0];var _s=UFO.createElement("style");if(!UFO.uaHas("ieWin")) _s.appendChild(document.createTextNode(selector+" {"+declaration+"}"));_s.setAttribute("type","text/css");_s.setAttribute("media","screen");_h.appendChild(_s);if(UFO.uaHas("ieWin")&&document.styleSheets&&document.styleSheets.length>0){var _ls=document.styleSheets[document.styleSheets.length-1];if(typeof _ls.addRule=="object") _ls.addRule(selector,declaration)}},setContainerCSS: function(id){var _fo=UFO.foList[id];var _w=/%/.test(_fo.width)?"":"px";var _h=/%/.test(_fo.height)?"":"px";UFO.createCSS("#"+id,"width:"+_fo.width+_w+"; height:"+_fo.height+_h+";");if(_fo.width=="100%"){UFO.createCSS("body","margin-left:0; margin-right:0; padding-left:0; padding-right:0;")}
if(_fo.height=="100%"){UFO.createCSS("html","height:100%; overflow:hidden;");UFO.createCSS("body","margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; height:100%;")}},createElement: function(el){return(UFO.uaHas("xml")&&typeof document.createElementNS!="undefined")?document.createElementNS("http://www.w3.org/1999/xhtml",el):document.createElement(el)},createObjParam: function(el,aName,aValue){var _p=UFO.createElement("param");_p.setAttribute("name",aName);_p.setAttribute("value",aValue);el.appendChild(_p)},uaHas: function(ft){var _u=UFO.ua;switch(ft){case "w3cdom":return(typeof document.getElementById!="undefined"&&typeof document.getElementsByTagName!="undefined"&&(typeof document.createElement!="undefined"||typeof document.createElementNS!="undefined"));case "xml":var _m=document.getElementsByTagName("meta");var _l=_m.length;for(var i=0;i<_l;i++){if (/content-type/i.test(_m[i].getAttribute("http-equiv"))&&/xml/i.test(_m[i].getAttribute("content"))) return true}
return false;case "ieMac":return/msie/.test(_u) && !/opera/.test(_u) && /mac/.test(_u);case "ieWin":return/msie/.test(_u) && !/opera/.test(_u) && /win/.test(_u);case "gecko":return/gecko/.test(_u) && !/applewebkit/.test(_u);case "opera":return/opera/.test(_u);case "safari":return/applewebkit/.test(_u);default:return false}},getFlashVersion: function(){if(UFO.fv[0]!=0) return;if(navigator.plugins&&typeof navigator.plugins["Shockwave Flash"]=="object"){UFO.pluginType="npapi";var _d=navigator.plugins["Shockwave Flash"].description;if(typeof _d!="undefined"){_d=_d.replace(/^.*\s+(\S+\s+\S+$)/,"$1");var _m=parseInt(_d.replace(/^(.*)\..*$/,"$1"),10);var _r=/r/.test(_d) ? parseInt(_d.replace(/^.*r(.*)$/,"$1"),10):0;UFO.fv=[_m,_r]}}
else if(window.ActiveXObject){UFO.pluginType="ax";try{var _a=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7")}
catch(e){try{var _a=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");UFO.fv=[6,0];_a.AllowScriptAccess="always"}
catch(e){if(UFO.fv[0]==6) return}
try{var _a=new ActiveXObject("ShockwaveFlash.ShockwaveFlash")}
catch(e){}}
if(typeof _a=="object"){var _d=_a.GetVariable("$version");if(typeof _d!="undefined"){_d=_d.replace(/^\S+\s+(.*)$/,"$1").split(",");UFO.fv=[parseInt(_d[0],10),parseInt(_d[2],10)]}}}},hasRequired: function(id){var _l=UFO.req.length;for(var i=0;i<_l;i++){if(typeof UFO.foList[id][UFO.req[i]]=="undefined") return false}
return true},hasFlashVersion: function(major,release){return(UFO.fv[0]>major||(UFO.fv[0]==major&&UFO.fv[1]>=release))?true:false},writeSWF: function(id){var _fo=UFO.foList[id];var _e=document.getElementById(id);if(UFO.pluginType=="npapi"){if(UFO.uaHas("gecko")||UFO.uaHas("xml")){while(_e.hasChildNodes()){_e.removeChild(_e.firstChild)}
var _obj=UFO.createElement("object");_obj.setAttribute("type","application/x-shockwave-flash");_obj.setAttribute("data",_fo.movie);_obj.setAttribute("width",_fo.width);_obj.setAttribute("height",_fo.height);var _l=UFO.optAtt.length;for(var i=0;i<_l;i++){if(typeof _fo[UFO.optAtt[i]]!="undefined") _obj.setAttribute(UFO.optAtt[i],_fo[UFO.optAtt[i]])}
var _o=UFO.opt.concat(UFO.optExc);var _l=_o.length;for(var i=0;i<_l;i++){if(typeof _fo[_o[i]]!="undefined") UFO.createObjParam(_obj,_o[i],_fo[_o[i]])}
_e.appendChild(_obj)}
else{var _emb="";var _o=UFO.opt.concat(UFO.optAtt).concat(UFO.optExc);var _l=_o.length;for(var i=0;i<_l;i++){if(typeof _fo[_o[i]]!="undefined") _emb+=' '+_o[i]+'="'+_fo[_o[i]]+'"'}
_e.innerHTML='<embed type="application/x-shockwave-flash" src="'+_fo.movie+'" width="'+_fo.width+'" height="'+_fo.height+'" pluginspage="http://www.macromedia.com/go/getflashplayer"'+_emb+'></embed>'}}
else if(UFO.pluginType=="ax"){var _objAtt="";var _l=UFO.optAtt.length;for(var i=0;i<_l;i++){if(typeof _fo[UFO.optAtt[i]]!="undefined") _objAtt+=' '+UFO.optAtt[i]+'="'+_fo[UFO.optAtt[i]]+'"'}
var _objPar="";var _l=UFO.opt.length;for(var i=0;i<_l;i++){if(typeof _fo[UFO.opt[i]]!="undefined") _objPar+='<param name="'+UFO.opt[i]+'" value="'+_fo[UFO.opt[i]]+'" />'}
var _p=window.location.protocol=="https:"?"https:":"http:";_e.innerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+_objAtt+' width="'+_fo.width+'" height="'+_fo.height+'" codebase="'+_p+'//download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version='+_fo.majorversion+',0,'+_fo.build+',0"><param name="movie" value="'+_fo.movie+'" />'+_objPar+'</object>'}},createDialog: function(id){var _fo=UFO.foList[id];UFO.createCSS("html","height:100%; overflow:hidden;");UFO.createCSS("body","height:100%; overflow:hidden;");UFO.createCSS("#xi-con","position:absolute; left:0; top:0; z-index:1000; width:100%; height:100%; background-color:#fff; filter:alpha(opacity:75); opacity:0.75;");UFO.createCSS("#xi-dia","position:absolute; left:50%; top:50%; margin-left: -"+Math.round(parseInt(_fo.xiwidth,10)/ 2) + "px; margin-top: -" + Math.round(parseInt(_fo.xiheight, 10) / 2)+"px; width:"+_fo.xiwidth+"px; height:"+_fo.xiheight+"px;");var _b=document.getElementsByTagName("body")[0];var _c=UFO.createElement("div");_c.setAttribute("id","xi-con");var _d=UFO.createElement("div");_d.setAttribute("id","xi-dia");_c.appendChild(_d);_b.appendChild(_c);var _mmu=window.location;if(UFO.uaHas("xml")&&UFO.uaHas("safari")){var _mmd=document.getElementsByTagName("title")[0].firstChild.nodeValue=document.getElementsByTagName("title")[0].firstChild.nodeValue.slice(0,47)+" - Flash Player Installation"}
else{var _mmd=document.title=document.title.slice(0,47)+" - Flash Player Installation"}
var _mmp=UFO.pluginType=="ax"?"ActiveX":"PlugIn";var _uc=typeof _fo.xiurlcancel!="undefined"?"&xiUrlCancel="+_fo.xiurlcancel:"";var _uf=typeof _fo.xiurlfailed!="undefined"?"&xiUrlFailed="+_fo.xiurlfailed:"";UFO.foList["xi-dia"]={movie:_fo.ximovie,width:_fo.xiwidth,height:_fo.xiheight,majorversion:"6",build:"65",flashvars:"MMredirectURL="+_mmu+"&MMplayerType="+_mmp+"&MMdoctitle="+_mmd+_uc+_uf};UFO.writeSWF("xi-dia")},expressInstallCallback: function(){var _b=document.getElementsByTagName("body")[0];var _c=document.getElementById("xi-con");_b.removeChild(_c);UFO.createCSS("body","height:auto; overflow:auto;");UFO.createCSS("html","height:auto; overflow:auto;")},cleanupIELeaks: function(){var _o=document.getElementsByTagName("object");var _l=_o.length
for(var i=0;i<_l;i++){_o[i].style.display="none";for(var x in _o[i]){if(typeof _o[i][x]=="function"){_o[i][x]=null}}}}};if(typeof window.attachEvent!="undefined"&&UFO.uaHas("ieWin")){window.attachEvent("onunload",UFO.cleanupIELeaks)}


/* - tabberoptions.js - */
// https://www.conhecimentos.cf/portal_javascripts/tabberoptions.js?original=1
var tabberOptions={manualStartup:true};

/* - tabber-minimizer.js - */
// https://www.conhecimentos.cf/portal_javascripts/tabber-minimizer.js?original=1
function tabberObj(argsObj){var arg;this.div=null;this.classMain="tabber";this.classMainLive="tabberlive";this.classTab="tabbertab";this.classTabDefault="tabbertabdefault";this.classNav="tabbernav";this.classTabHide="tabbertabhide";this.classNavActive="tabberactive";this.titleElements=['h2','h3','h4','h5','h6'];this.titleElementsStripHTML=true;this.removeTitle=true;this.addLinkId=true;this.linkIdFormat='<tabberid>nav<tabnumberone>';for(arg in argsObj){this[arg]=argsObj[arg]}
this.REclassMain=new RegExp('\\b'+this.classMain+'\\b','gi');this.REclassMainLive=new RegExp('\\b'+this.classMainLive+'\\b','gi');this.REclassTab=new RegExp('\\b'+this.classTab+'\\b','gi');this.REclassTabDefault=new RegExp('\\b'+this.classTabDefault+'\\b','gi');this.REclassTabHide=new RegExp('\\b'+this.classTabHide+'\\b','gi');this.tabs=new Array();if(this.div){this.init(this.div);this.div=null}}
tabberObj.prototype.init=function(e){var childNodes,i,i2,t,defaultTab=0,DOM_ul,DOM_li,DOM_a,aId,headingElement;if(!document.getElementsByTagName){return false}
if(e.id){this.id=e.id}
this.tabs.length=0;childNodes=e.childNodes;for(i=0;i<childNodes.length;i++){if(childNodes[i].className&&childNodes[i].className.match(this.REclassTab)){t=new Object();t.div=childNodes[i];this.tabs[this.tabs.length]=t;if(childNodes[i].className.match(this.REclassTabDefault)){defaultTab=this.tabs.length-1}}}
DOM_ul=document.createElement("ul");DOM_ul.className=this.classNav;for(i=0;i<this.tabs.length;i++){t=this.tabs[i];t.headingText=t.div.title;if(this.removeTitle){t.div.title=''}
if(!t.headingText){for(i2=0;i2<this.titleElements.length;i2++){headingElement=t.div.getElementsByTagName(this.titleElements[i2])[0];if(headingElement){t.headingText=headingElement.innerHTML;if(this.titleElementsStripHTML){t.headingText.replace(/<br>/gi," ");t.headingText=t.headingText.replace(/<[^>]+>/g,"")}
break}}}
if(!t.headingText){t.headingText=i+1}
DOM_li=document.createElement("li");t.li=DOM_li;DOM_a=document.createElement("a");DOM_a.appendChild(document.createTextNode(t.headingText));DOM_a.href="javascript:void(null);";DOM_a.title=t.headingText;DOM_a.onclick=this.navClick;DOM_a.tabber=this;DOM_a.tabberIndex=i;if(this.addLinkId&&this.linkIdFormat){aId=this.linkIdFormat;aId=aId.replace(/<tabberid>/gi,this.id);aId=aId.replace(/<tabnumberzero>/gi,i);aId=aId.replace(/<tabnumberone>/gi,i+1);aId=aId.replace(/<tabtitle>/gi,t.headingText.replace(/[^a-zA-Z0-9\-]/gi,''));DOM_a.id=aId}
DOM_li.appendChild(DOM_a);DOM_ul.appendChild(DOM_li)}
e.insertBefore(DOM_ul,e.firstChild);e.className=e.className.replace(this.REclassMain,this.classMainLive);this.tabShow(defaultTab);if(typeof this.onLoad=='function'){this.onLoad({tabber:this})}
return this};tabberObj.prototype.navClick=function(event){var rVal,a,self,tabberIndex,onClickArgs;a=this;if(!a.tabber){return false}
self=a.tabber;tabberIndex=a.tabberIndex;a.blur();if(typeof self.onClick=='function'){onClickArgs={'tabber':self,'index':tabberIndex,'event':event};if(!event){onClickArgs.event=window.event}
rVal=self.onClick(onClickArgs);if(rVal===false){return false}}
self.tabShow(tabberIndex);return false};tabberObj.prototype.tabHideAll=function(){var i;for(i=0;i<this.tabs.length;i++){this.tabHide(i)}};tabberObj.prototype.tabHide=function(tabberIndex){var div;if(!this.tabs[tabberIndex]){return false}
div=this.tabs[tabberIndex].div;if(!div.className.match(this.REclassTabHide)){div.className+=' '+this.classTabHide}
this.navClearActive(tabberIndex);return this};tabberObj.prototype.tabShow=function(tabberIndex){var div;if(!this.tabs[tabberIndex]){return false}
this.tabHideAll();div=this.tabs[tabberIndex].div;div.className=div.className.replace(this.REclassTabHide,'');this.navSetActive(tabberIndex);if(typeof this.onTabDisplay=='function'){this.onTabDisplay({'tabber':this,'index':tabberIndex})}
return this};tabberObj.prototype.navSetActive=function(tabberIndex){this.tabs[tabberIndex].li.className=this.classNavActive;return this};tabberObj.prototype.navClearActive=function(tabberIndex){this.tabs[tabberIndex].li.className='';return this};
function tabberAutomatic(tabberArgs){var tempObj,divs,i;if(!tabberArgs){tabberArgs={}}
tempObj=new tabberObj(tabberArgs);divs=document.getElementsByTagName("div");for(i=0;i<divs.length;i++){if(divs[i].className&&divs[i].className.match(tempObj.REclassMain)){tabberArgs.div=divs[i];divs[i].tabber=new tabberObj(tabberArgs)}}
return this}
function tabberAutomaticOnLoad(tabberArgs){var oldOnLoad;if(!tabberArgs){tabberArgs={}}
oldOnLoad=window.onload;if(typeof window.onload!='function'){window.onload=function(){tabberAutomatic(tabberArgs)}}
else{window.onload=function(){oldOnLoad();tabberAutomatic(tabberArgs)}}}
if(typeof tabberOptions=='undefined'){tabberAutomaticOnLoad()}else{if(!tabberOptions['manualStartup']){tabberAutomaticOnLoad(tabberOptions)}}

/* - loadanalytics.js - */
// https://www.conhecimentos.cf/portal_javascripts/loadanalytics.js?original=1
jQuery(document).ready(
function(){try{jQuery.ga.load('UA-1592958-1');jQuery(function(){jQuery('a').click(function(){var a=jQuery(this);var href=a.attr('href');var event='click';var category=a.parents('[id!=""]:first').get(0).id;var label=((a.text()||a.children('img:first').attr('alt'))+'('+href+')');if((href.match(/^http/i))&&(!href.match(document.domain))){jQuery.ga.trackEvent(category+'( Url Externa )',event,label)} else if((!href.match(/\.(doc|pdf|xls|ppt|zip|txt|vsd|vxd|js|css|rar|exe|wma|mov|avi|wmv|mp3|tar|gz|bz2)$/i) ) && (! href.match(/^mailto:/i))){jQuery.ga.trackEvent(category+'( Url Interna )',event,label)} else{if(href.match(/\.(doc|pdf|xls|ppt|zip|txt|vsd|vxd|js|css|rar|exe|wma|mov|avi|wmv|mp3|tar|gz|bz2)$/i)){jQuery.ga.trackEvent(category+'( Download )',event,label)} else{if(href.match(/^mailto:/i)){jQuery.ga.trackEvent(category+'(Mailto)',event,label)}}}})})}
catch(err){}
var date=new Date();var curDate=null;do{curDate=new Date()} while(curDate-date<300)});

/* - tabber_inline.js - */
// https://www.conhecimentos.cf/portal_javascripts/tabber_inline.js?original=1
var tabberOptions={manualStartup:true};/* \x3C is the hexcode for 'less than'(&lt;) \x3E is the hexcode for 'greater than'(&gt;) it's needed for proper XHTML validation */ document.write('\x3Cstyle type="text/css"\x3E.tabber{display:none}\x3C/style\x3E');

/* - jquery.ga.js - */
// https://www.conhecimentos.cf/portal_javascripts/jquery.ga.js?original=1
(function($){jQuery.ga={};jQuery.ga.load=function(uid,callback){jQuery.ajax({type:'GET',url:(document.location.protocol=="https:"?"https://ssl":"http://www")+'.google-analytics.com/ga.js',cache:true,success: function(){if(typeof _gat==undefined){throw "_gat has not been defined"}
t=_gat._getTracker(uid);bind();if(jQuery.isFunction(callback)){callback(t)}
t._trackPageview();t._trackPageLoadTime()},dataType:'script',data:null})};var t;var bind=function(){if(noT()){throw "pageTracker has not been defined"}
for(var $1 in t){if($1.charAt(0)!='_') continue;jQuery.ga[$1.substr(1)]=t[$1]}};var noT=function(){return t==undefined}})(jQuery);
